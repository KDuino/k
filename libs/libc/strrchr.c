#include <string.h>

char *strrchr(const char *s, int c) {
    const char *d = s;
    while (*d != '\0') {
        d++;
    }
    do {
        if (*d == c) {
            return d;
        }
    } while (d-- != s);
    return NULL;
}