#include <ctype.h>

int isspace(int c) {
  return c == ' ' || c == '\f' || c == '\n' || c == '\r' || c == '\t' || c == '\v';
}

int isalpha(int c) {
  return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
}

int isdigit(c) {
  return c >= '0' && c <= '9';
}

int isalnum(int c) {
  return isalpha(c) || isdigit(c);
}

int iscntrl(int c) {
  return (c >= 0 && c <= 31) || c == 127;
}

int isascii(int c) {
  return c >= 0 && c <= 127;
}

int isblank(int c) {
  return c == ' ' || c == '\t';
}

int isgraph(int c) {
  return c >= '!' && c <= '~' && c != ' ';
}

int islower(int c) {
  return c >= 'a' && c <= 'z';
}

int isprint(int c) {
  return c >= '!' && c <= '~';
}

int ispunct(int c) {
  return isprint(c) && !isalnum(c) && c != ' ';
}

int isupper(int c) {
  return c >= 'A' && c <= 'Z';
}

int isxdigit(int c) {
  return isdigit(c) || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F');
}

int toascii(int c) {
  return c & 0b1111111;
}