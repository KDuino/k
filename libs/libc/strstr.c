#include <string.h>

char *strstr(const char *havstack, const char *needle) {
    size_t n = strlen(needle);
    while (*havstack != '\0') {
        if (!strncmp(havstack, needle, n)) {
            return havstack;
        }
        havstack++;
    }
    return NULL;
}