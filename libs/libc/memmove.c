#include <stdint.h>
#include <stddef.h>
#include <string.h>

void *memmove(void *dest, const void *src, size_t n) {
    uint8_t tmp[n];
    uint8_t *ptr = dest;
    uint8_t *u = src;
    for (size_t i = 0; i < n; i++) {
        tmp[i] = u[i];
    }
    for (size_t i = 0; i < n; i++) {
        ptr[i] = tmp[i];
    }
    return dest;
}