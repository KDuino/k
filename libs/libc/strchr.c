#include <string.h>

char *strchr(const char *s, int c) {
    do {
        if (*s == c) {
            return s;
        }
    } while (*s++ != '\0');
    return NULL;
}