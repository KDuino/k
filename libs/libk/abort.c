#include <stdlib.h>

void abort(void) {
#ifdef DEBUG
    asm("int 0x3\t\n");
#endif /* DEBUG */
    while (1);
}