#include <stdbool.h>
#include <k/kstd.h>
#include <k/kfs.h>

volatile int k_open(const char *pathname, int mode) {
  if (pathname == NULL || mode != O_RDONLY || fds == NULL) {
    return -1;
  }
  struct kfs_inode *inode = (struct kfs_inode *)((size_t)sblk + (KFS_BLK_SZ * sblk->inode_idx));
  for (size_t i = 0; i < sblk->inode_cnt; i++) {
    if (!strcmp(fds[i].ino->filename, pathname)) {
      if (!fds[i].opened) {
        fds[i].opened = true;
        return i;
      } else {
        return -1;
      }
    }
    inode = (struct kfs_inode *)((size_t)sblk + (KFS_BLK_SZ * inode->next_inode));
  }
  return -1;
}

volatile int k_close(int fd) {
  if (fd >= sblk->inode_cnt) {
    return -1;
  }
  fds[fd].opened = false;
  fds[fd].offset = 0;
  fds[fd].idx = 0;
  fds[fd].blk = 0;
  fds[fd].iblk = NULL;
  return 0;
}

volatile static int k_read_byte(int fd, uint8_t *dest) {
  if (fds[fd].blk < fds[fd].ino->d_blk_cnt) {
    struct kfs_block *block = (struct kfs_block *)((size_t)sblk + (KFS_BLK_SZ * fds[fd].ino->d_blks[fds[fd].blk]));
    *dest = block->data[fds[fd].idx];
    fds[fd].idx++;
    fds[fd].offset++;
    if (fds[fd].idx >= KFS_BLK_DATA_SZ) {
      fds[fd].idx = 0;
      fds[fd].blk++;
    }
    return 0;
  } else if ((fds[fd].blk - fds[fd].ino->d_blk_cnt) < fds[fd].ino->i_blk_cnt) {
    struct kfs_iblock *iblock = (struct kfs_iblock *)((size_t)sblk + (KFS_BLK_SZ * fds[fd].ino->i_blks[fds[fd].blk - fds[fd].ino->d_blk_cnt]));
    size_t iblock_dblock = fds[fd].idx / KFS_BLK_DATA_SZ;
    size_t dblock_offset = fds[fd].idx % KFS_BLK_DATA_SZ;
    struct kfs_block *block = (struct kfs_block *)((size_t)sblk + (KFS_BLK_SZ * iblock->blks[iblock_dblock]));
    *dest = block->data[dblock_offset];
    fds[fd].idx++;
    fds[fd].offset++;
    if (!(fds[fd].idx % KFS_BLK_DATA_SZ)) {
      fds[fd].blk++;
    }
    return 0;
  }
  return -1;
}

volatile ssize_t k_read(int fd, void *buf, size_t count) {
  if (fd >= sblk->inode_cnt || !fds[fd].opened || buf == NULL) {
    return -1;
  }
  uint8_t *data = (uint8_t *)buf;
  size_t i;
  for (i = 0; i < count && fds[fd].offset < fds[fd].ino->file_sz; i++) {
    if (k_read_byte(fd, &(data[i]))) {
      break;
    }
  }
  return i;
}

volatile off_t k_seek(int fd, off_t offset, int whence) {
  if (offset < 0) {
    printf("Warn: invalid offset\n");
    return -1;
  } else if (whence == SEEK_SET) {
    fds[fd].offset = 0;
    fds[fd].idx = 0;
    fds[fd].blk = 0;
    fds[fd].iblk = NULL;
    return k_seek(fd, offset, SEEK_CUR);
  } else if (whence == SEEK_CUR) {
    off_t i;
    uint8_t tmp;
    for (i = 0; i < offset; i++) {
      if (k_read_byte(fd, &tmp)) {
        break;
      }
    }
    return fds[fd].offset;
  } else if (whence == SEEK_END) {
    uint8_t tmp;
    while (!k_read_byte(fd, &tmp));
    return fds[fd].offset;
  } else {
    printf("Warn: unknown command %d\n", whence);
    return -1;
  }
}