#include <stdlib.h>
#include <string.h>

void *memset(void *s, int c, size_t n)
{
	char *p = s;

	for (size_t i = 0; i < n; ++i)
		p[i] = c;

	return s;
}

void *calloc(size_t nmemb, size_t size)
{
	void *p = malloc(size * nmemb);

	if (!p)
		return NULL;

	memset(p, 0, nmemb * size);

	return (p);
}
