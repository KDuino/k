#include <k/kstd.h>
#include <x86/macros.h>
#include <io.h>

int k_write(const char *buffer, size_t count) {
  int val = count;
  while (count--) outb(COM1, *buffer++);
  return val;
}