CFLAGS	= -std=gnu99 -O0 -Wall -Wextra -nostdinc -fno-builtin -ffreestanding \
	  -m32 -fno-asynchronous-unwind-tables -fno-common -static
# SSP causes compilation problems on Ubuntu
CFLAGS	+= -fno-stack-protector
#K_EXTRA_CFLAGS = -g3
#CFLAGS += -ffunction-sections -fdata-sections
CXXFLAGS = -std=gnu++11 -Os -Wall -Wextra -nostdlib -ffreestanding -m32 -fno-exceptions -fno-asynchronous-unwind-tables -fno-unwind-tables -fno-rtti -fno-common -static
CPPFLAGS += -I$(shell $(CC) -m32 --print-file-name=include)

ASFLAGS = -m32
LDFLAGS	= -nostdlib -m32 -Wl,--build-id=none -nostartfiles
#LDFLAGS += -Wl,--gc-sections -Wl,--print-gc-sections
ARFLAGS = src
