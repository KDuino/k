#include <stdint.h>
#include <stdio.h>
#include <x86/macros.h>
#include <x86/interrupts.h>
#include <k/kstd.h>
#include <x86/keyboard.h>
#include <x86/stdio.h>
#include "io.h"

volatile pic_callback callbacks[16] = {
  pit_controller, // System Timer
  keyboard_controller, // Keyboard Controller
  NULL, // Cascaded Signals
  NULL, // Serial Port Controllers 2 and 4
  NULL, // Serial Port Controllers 1 and 3
  NULL, // Parallel Port 2 and 3 || Sound card
  NULL, // Floppy Disk Controller
  NULL, // Parallel Port 1
  NULL, // RTC
  NULL, // ACPI
  NULL, // Available
  NULL, // Available
  NULL, // Mouse Controller
  NULL, // CPU Co-processor
  NULL, // ATA1
  NULL // ATA2
};

static volatile uint32_t write_syscall_wrapper(int syscall, const void *s, size_t length, ...) {
  (void)syscall;
  return (uint32_t) k_write(s, length);
}

static volatile uint32_t sbrk_syscall_wrapper(int syscall, ssize_t increment, ...) {
  (void)syscall;
  if (increment <= 0) {
    return brk;
  } else {
    void *ptr = brk;
    brk += increment;
    return ptr;
  }
}

static volatile int getkey_syscall_wrapper(int syscall, ...) {
  (void)syscall;
  return k_getkey();
}

static volatile unsigned int gettick_syscall_wrapper(int syscall, ...) {
  (void)syscall;
  return millis() / 10;
}

static volatile int open_syscall_wrapper(int syscall, const char *pathname, int flags, ...) {
  (void)syscall;
  return k_open(pathname, flags);
}

static volatile int close_syscall_wrapper(int syscall, int fd, ...) {
  (void)syscall;
  return k_close(fd);
}

static volatile ssize_t read_syscall_wrapper(int syscall, int fd, void *buf, size_t count, ...) {
  (void)syscall;
  return k_read(fd, buf, count);
}

static volatile off_t seek_syscall_wrapper(int syscall, int fd, off_t offset, int whence, ...) {
  (void)syscall;
  return k_seek(fd, offset, whence);
}

volatile syscall_callback syscalls[NR_SYSCALL] = {
  NULL, // NULL syscall
  (syscall_callback)write_syscall_wrapper, // write syscall
  (syscall_callback)sbrk_syscall_wrapper, // sbrk syscall
  (syscall_callback)getkey_syscall_wrapper, // getkey syscall 
  (syscall_callback)gettick_syscall_wrapper, // gettick syscall
  (syscall_callback)open_syscall_wrapper, // open syscall
  (syscall_callback)read_syscall_wrapper, // read syscall
  (syscall_callback)seek_syscall_wrapper, // seek syscall
  (syscall_callback)close_syscall_wrapper, // close syscall
  NULL, // setvideo syscall
  NULL, // swap_frontbuffer syscall
  NULL, // playsound syscall
  NULL, // getmouse syscall
  NULL // getkeymode syscall
};

volatile uint32_t end_of_interrupt(uint8_t irq, uint32_t *sp) {
  uint32_t ret = 0;
  if (irq != NOP_IRQ) {
    if (irq == SYSCALL_IRQ) {
      int invoked_syscall = *(sp + 1);
      uint32_t ebx, ecx, edx;
      asm volatile("mov %%ebx, %0\n" : "=r"(ebx) : : "%ebx");
      asm volatile("mov %%ecx, %0\n" : "=r"(ecx) : : "%ecx");
      asm volatile("mov %%edx, %0\n" : "=r"(edx) : : "%edx");
      if (invoked_syscall > 0 && invoked_syscall < NR_SYSCALL && syscalls[invoked_syscall] != NULL) {
        ret = syscalls[invoked_syscall](invoked_syscall, ebx, ecx, edx); // TODO
      }
    } else {
      callbacks[irq - IRQ_OFFSET]();
    }
  }
  if (irq >= IRQ8 && irq <= IRQ15) {
    outb(PIC_SLAVE_PORT_A, PIC_END_OF_INTERRUPT);
  }
  outb(PIC_MASTER_PORT_A, PIC_END_OF_INTERRUPT);
  return ret;
}

volatile uint32_t interrupt_handler(uint32_t *sp) {
  uint32_t irq = *sp;
  if (irq <= 255) {
    return end_of_interrupt(irq, sp);
  } else {
    return end_of_interrupt(NOP_IRQ, sp);
  }
}
