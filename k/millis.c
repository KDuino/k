#include <stdio.h>

static volatile unsigned long ticked_millis = 0;

volatile void pit_controller(void) {
  ticked_millis += 10;
}

volatile unsigned long millis(void) {
  return ticked_millis;
}