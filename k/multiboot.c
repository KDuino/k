#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include "../libs/libk/include/stdlib.h"
#include "multiboot.h"
#include "include/k/kfs.h"
#include "k/kstd.h"
#define NULL (void*)0
#define SFLAG(flag) {flag, #flag}

typedef struct sflag {
  uint32_t mask;
  const char *str;
} sflag;

static volatile sflag str_flags[13] = {
  SFLAG(MULTIBOOT_INFO_MEMORY),
  SFLAG(MULTIBOOT_INFO_BOOTDEV),
  SFLAG(MULTIBOOT_INFO_CMDLINE),
  SFLAG(MULTIBOOT_INFO_MODS),
  SFLAG(MULTIBOOT_INFO_AOUT_SYMS),
  SFLAG(MULTIBOOT_INFO_ELF_SHDR),
  SFLAG(MULTIBOOT_INFO_MEM_MAP),
  SFLAG(MULTIBOOT_INFO_DRIVE_INFO),
  SFLAG(MULTIBOOT_INFO_CONFIG_TABLE),
  SFLAG(MULTIBOOT_INFO_BOOT_LOADER_NAME),
  SFLAG(MULTIBOOT_INFO_APM_TABLE),
  SFLAG(MULTIBOOT_INFO_VBE_INFO),
  SFLAG(MULTIBOOT_INFO_FRAMEBUFFER_INFO)
};

volatile void     *brk = NULL;
volatile kfs_file *fds = NULL;
volatile struct kfs_superblock *sblk = NULL;

static inline void print_str_flags(uint32_t flags) {
  for (size_t i = 0; i < 13; i++) {
    if (str_flags[i].mask & flags) {
      printf("Flag found: %s\n", str_flags[i].str);
    }
  }
}

static void init_superblock(struct kfs_superblock *ptr) {
  if (ptr == NULL) {
    return;
  }
  sblk = ptr;
  if (sblk->magic != KFS_MAGIC) {
    printf("Superblock wrong magic number, aborting mounting kfs");
    return;
  }
  fds = calloc(sblk->inode_cnt, sizeof(struct kfs_file));
  struct kfs_inode *inode = (struct kfs_inode *)((size_t)sblk + (KFS_BLK_SZ * sblk->inode_idx));
  for (size_t i = 0; i < sblk->inode_cnt; i++) {
    fds[i].ino = inode;
    inode = (struct kfs_inode *)((size_t)sblk + (KFS_BLK_SZ * inode->next_inode));
  }
  /* printf("Name: %s\n", sblk->name);
  printf("Inode count: %u\n", sblk->inode_cnt);
  printf("First inode address: %p\n", (void *)sblk->inode_idx);
  printf("Number of blocks: %u\n", sblk->blk_cnt);
  printf("Superblock creation date: %u\n", sblk->ctime);
  printf("Checksum: %u\n", sblk->cksum);
  printf("Computed Checksum: %u\n", kfs_checksum(sblk, sizeof(struct kfs_superblock) - 4)); */
}

/*static inline void search_first_inode(struct kfs_superblock *sblk) {
  struct kfs_inode *inode = (struct kfs_inode *)((size_t)sblk + (KFS_BLK_SZ * sblk->inode_idx));
  for (size_t i = 0; i < sblk->inode_cnt; i++) {
    printf("Inode is at address %p\n", inode);
    printf("Inode filename = %s\n", inode->filename);
    printf("Inode file size = %u bytes\n", inode->file_sz);
    printf("Inode checksum = %u\n", kfs_checksum(inode, sizeof(struct kfs_inode) - 4));
    printf("Inode expected checksum = %u\n", inode->cksum);
    printf("Inode index = %u\n", inode->idx);
    printf("Inode next inode = %p\n", inode->next_inode);
    for (size_t i = 0; i < inode->d_blk_cnt; i++) {
      printf("Inode direct block %lu address = %x\n", i, inode->d_blks[i]);
      struct kfs_block *block = (struct kfs_block *)((size_t)sblk + (KFS_BLK_SZ * inode->d_blks[i]));
      printf("Direct block indicated index = %x\n", block->idx);
      printf("Direct block checksum = %u ; Expected checksum = %u\n", kfs_checksum(&(block->data), KFS_BLK_DATA_SZ), block->cksum);
    }
    printf("Number of indirect blocks %u\n", inode->i_blk_cnt);
    for (size_t i = 0; i < inode->i_blk_cnt; i++) {
      printf("Inode indirect block %lu address = %x\n", i, inode->i_blks[i]);
      struct kfs_iblock *iblock = (struct kfs_iblock *)((size_t)sblk + (KFS_BLK_SZ * inode->i_blks[i]));
      printf("Indirect block indicated index = %x\n", iblock->idx);
      printf("Indirect block indicated data block count = %u\n", iblock->blk_cnt);
      printf("Indirect block checksum = %u ; expected checksum = %u\n", kfs_checksum(iblock, sizeof(struct kfs_iblock) - 4), iblock->cksum);
      for (size_t i = 0; i < iblock->blk_cnt; i++) {
        struct kfs_block *block = (struct kfs_block *)((size_t)sblk + (KFS_BLK_SZ * iblock->blks[i]));
        printf("Direct block indicated index = %x\n", block->idx);
        printf("Direct block checksum = %u ; Expected checksum = %u\n", kfs_checksum(&(block->data), KFS_BLK_DATA_SZ), block->cksum);
      }
    }
    inode = (struct kfs_inode *)((size_t)sblk + (KFS_BLK_SZ * inode->next_inode));
  }
}*/

volatile void mount_kfs(multiboot_info_t *info) {
  module_t *mod_sblk = (module_t *)info->mods_addr;
  struct kfs_superblock *sblk = (struct kfs_superblock *)mod_sblk->mod_start;
  init_superblock(sblk);
}

volatile static k_boot_info_t boot = {0, NULL, NULL};

volatile void handle_multiboot(multiboot_info_t *info) {
  if (info == 0) {
    return;
  }
  if (info->flags & MULTIBOOT_INFO_BOOTDEV) {
    boot.disk = info->boot_device;
  }
  if (info->flags & MULTIBOOT_INFO_CMDLINE) {
    boot.cmd = info->cmdline;
  }
  if (info->flags & MULTIBOOT_INFO_MODS) {
    boot.sblk = (struct kfs_superblock *)((module_t *)info->mods_addr)->mod_start;
  }
  module_t *mod = (module_t *)info->mods_addr;
  for (size_t i = 0; i < info->mods_count; i++) {
    mod = (module_t *)mod->mod_end;
  }
  brk = (void *)((size_t)mod + 1048576L);
  mount_kfs(info);
}