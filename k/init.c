#include <string.h>
#include <x86/intel_types.h>
#include <x86/macros.h>
#include "io.h"

void init_PIC(void) {
  outb(PIC_MASTER_PORT_A, DECLARE_ICW1_MASTER);
  outb(PIC_SLAVE_PORT_A, DECLARE_ICW1_SLAVE);
  outb(PIC_MASTER_PORT_B, DECLARE_ICW2_MASTER);
  outb(PIC_SLAVE_PORT_B, DECLARE_ICW2_SLAVE);
  outb(PIC_MASTER_PORT_B, DECLARE_ICW3_MASTER);
  outb(PIC_SLAVE_PORT_B, DECLARE_ICW3_SLAVE);
  outb(PIC_MASTER_PORT_B, DECLARE_ICW4_8086);
  outb(PIC_SLAVE_PORT_B, DECLARE_ICW4_8086);
  // Debug masks:
  outb(PIC_MASTER_PORT_B, 0xFC);
  outb(PIC_SLAVE_PORT_B, 0xFF);
}

void init_serial(void) {
  outb(COM1 + 3, 0b1000011);
  outb(COM1 + 2, 0b11000111);
  outb(COM1, 0x1);
  outb(COM1 + 1, 0x0);
  outb(COM1 + 3, 0b00000011);
}

void init_PIT(void) {
  outb(PIT_COMMAND_PORT, PIT_CHANNEL0_INIT);
  outb(PIT_CHANNEL0_DATA_PORT, PIT_CHANNEL0_DIVIDER & 0xFF);
  outb(PIT_CHANNEL0_DATA_PORT, PIT_CHANNEL0_DIVIDER & 0xFF00);
}

extern uint32_t nop_isr;
extern uint32_t keyboard_isr;
extern uint32_t pit_isr;

void init_idt_table(volatile IDT *idt_table, size_t n) {
  uint32_t offset;
  asm volatile("mov $nop_isr, %0\n" : "=r"(offset));
  IDT global_isr = {offset & 0xFFFF, 0x8, 0x0, 0xE, 0, 0, 1, offset >> 16};
  for (size_t i = 0; i < n; i++) {
    memcpy((void *)&idt_table[i], &global_isr, sizeof(IDT));
  }
}

void init_idt_keyboard(volatile IDT *idt_table) {
  uint32_t offset;
  asm volatile("mov $keyboard_isr, %0\n" : "=r"(offset));
  idt_table[33].offset1 = offset & 0xFFFF;
  idt_table[33].offset2 = offset >> 16;
}

void init_idt_pit(volatile IDT *idt_table) {
  uint32_t offset;
  asm volatile("mov $pit_isr, %0\n" : "=r"(offset));
  idt_table[32].offset1 = offset & 0xFFFF;
  idt_table[32].offset2 = offset >> 16;
}

void init_idt_syscall(volatile IDT *idt_table) {
  uint32_t offset;
  asm volatile("mov $syscall_isr, %0\n" : "=r"(offset));
  idt_table[128].offset1 = offset & 0xFFFF;
  idt_table[128].offset2 = offset >> 16;
}

void load_idtr(volatile const IDTR *idtr) {
  asm volatile("lidt %0\n" : : "m"(*idtr) : "memory");
}

void init_isr(void) {
  static volatile IDT idt_table[IDT_NB_ELEMENTS];
  static volatile IDTR idtr = {sizeof(idt_table) - 1, 0, 0};

  init_idt_table(idt_table, IDT_NB_ELEMENTS);
  init_idt_keyboard(idt_table);
  init_idt_pit(idt_table);
  init_idt_syscall(idt_table);
  idtr.base = (uint32_t)&idt_table;
  load_idtr(&idtr);
  return;
}

void init_pe_mode(void) {
  // Set PE bit
  asm volatile(
      "mov %cr0, %eax\n"
      "or $1, %eax\n"
      "mov %eax, %cr0");
  // Context switch to enable PE mode
  asm volatile(
      "push $0x8\n"
      "push $k2\n"
      "lret\n"
      "k2:");
  // Reload segment registers
  asm volatile(
      "mov $0x10, %ax\n"
      "mov %ax, %ss\n"
      "mov %ax, %ds\n"
      "mov %ax, %es\n"
      "mov %ax, %fs\n"
      "mov %ax, %gs\n");
}

void init_gdt(void) {
  static volatile const GDT gdt_table[] = {
      NULL_SEGMENT, KCODE_SEGMENT, KDATA_SEGMENT, UCODE_SEGMENT, UDATA_SEGMENT};

  GDTR gdtr = {sizeof(gdt_table) - 1, (uint32_t)&gdt_table, 0};
  // Clear DS register
  asm volatile(
      "mov $0, %eax\n"
      "mov %eax, %ds\n");
  asm volatile("lgdt %0\n" : : "m"(gdtr) : "memory");
}