#ifndef IO_H_
#define IO_H_

#include <k/types.h>

static inline void outb(u16 port, u8 val) {
  asm volatile("outb %0, %1\n\t"
               : /* No output */
               : "a"(val), "d"(port));

  // FIXME
}

static inline u8 inb(u16 port) {
  u8 b;

  asm volatile(
      "mov %1, %%dx\n"
      "inb %%dx, %%al\n"
      "mov %%al, %0\n"
      : "=r"(b)
      : "r"(port)
      : "%dx");

  return b;
}

#endif /* !IO_H_ */
