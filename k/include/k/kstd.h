/*
* Copyright (c) LSE
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY LSE AS IS AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL LSE BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef K_KSTD_H_
#define K_KSTD_H_

#include <k/types.h>

typedef s32 ssize_t;
typedef s32 off_t;

struct melody {
	unsigned long freq;
	unsigned long duration;
};

/*
** constants
*/

/* console */
enum e_cons_codes {
	CONS_ESCAPE = 255,
	CONS_CLEAR = 1,
	CONS_COLOR = 2,
	CONS_SETX = 3,
	CONS_SETY = 4,
	CONS_BLACK = 0,
	CONS_BLUE = 1,
	CONS_GREEN = 2,
	CONS_CYAN = 3,
	CONS_RED = 4,
	CONS_MAGENTA = 5,
	CONS_YELLOW = 6,
	CONS_WHITE = 7,
	CONS_BLINK = (1 << 7),
	CONS_LIGHT = (1 << 3)
};

#define CONS_FRONT(Color)	(Color)
#define CONS_BACK(Color)	(Color << 4)

/* keyboard */
enum e_kbd_codes {
	KEY_ESC = 1,
  KEY_1 = 2,
	KEY_2 = 3,
	KEY_3 = 4,
	KEY_4 = 5,
	KEY_5 = 6,
	KEY_6 = 7,
	KEY_7 = 8,
	KEY_8 = 9,
	KEY_9 = 10,
	KEY_0 = 11,
  KEY_MINUS_SIGN = 12,
  KEY_EQUAL_SIGN = 13,
  KEY_BACKSPACE = 14,
  KEY_TAB = 15,
  KEY_Q = 16,
  KEY_W = 17,
  KEY_E = 18,
  KEY_R = 19,
  KEY_T = 20,
  KEY_Y = 21,
  KEY_U = 22,
  KEY_I = 23,
  KEY_O = 24,
  KEY_P = 25,
  KEY_OPEN_BRACKET = 26,
  KEY_CLOSE_BRACKET = 27,
  KEY_ENTER = 28,
  KEY_CTRL = 29,
  KEY_A = 30,
  KEY_S = 31,
  KEY_D = 32,
  KEY_F = 33,
  KEY_G = 34,
  KEY_H = 35,
  KEY_J = 36,
  KEY_K = 37,
  KEY_L = 38,
  KEY_SEMICOLON = 39,
  KEY_SINGLE_QUOTE = 40,
  KEY_BACK_TICK = 41,
  KEY_LSHIFT = 42,
  KEY_BACK_SLASH = 43,
  KEY_Z = 44,
  KEY_X = 45,
  KEY_C = 46,
  KEY_V = 47,
  KEY_B = 48,
  KEY_N = 49,
  KEY_M = 50,
  KEY_COLON = 51,
  KEY_DOT = 52,
  KEY_SLASH = 53,
  KEY_RSHIFT = 54,
  KEY_SYST = 55,
  KEY_ALT = 56,
  KEY_SPACE = 57,
  KEY_MAJLOCK = 58,
	KEY_F1 = 59,
	KEY_F2 = 60,
	KEY_F3 = 61,
	KEY_F4 = 62,
	KEY_F5 = 63,
	KEY_F6 = 64,
	KEY_F7 = 65,
	KEY_F8 = 66,
	KEY_f9 = 67,
	KEY_F10 = 68,
  KEY_PAUSE = 69,
  KEY_SCROLL_LOCK = 70,
  KEY_PAD_7 = 71,
  KEY_UP = 72,
  KEY_PAD_9 = 73,
  KEY_PAD_MINUS_SIGN = 74,
  KEY_LEFT = 75,
  KEY_PAD_5 = 76,
  KEY_RIGHT = 77,
  KEY_PAD_PLUS_SIGN = 78,
  KEY_PAD_1 = 79,
  KEY_DOWN = 80,
  KEY_PAD_3 = 81,
  KEY_INSER = 82,
	KEY_SUPPR = 83,
	KEY_F11 = 87,
	KEY_F12 = 88
};

enum e_k_mode {
	KEY_PRESSED,
	KEY_RELEASED,
};

/* mouse */
enum e_mouse_codes {
	BUTTON_LEFT = 1,
	BUTTON_RIGHT = 2
};

/* misc */
#define O_RDONLY	0
#define SEEK_SET	0
#define SEEK_CUR	1
#define SEEK_END	2
#define VIDEO_GRAPHIC	0
#define VIDEO_TEXT	1

/*
** syscalls
*/

#define SYSCALL_WRITE			1
#define SYSCALL_SBRK			2
#define SYSCALL_GETKEY			3
#define SYSCALL_GETTICK			4
#define SYSCALL_OPEN			5
#define SYSCALL_READ			6
#define SYSCALL_SEEK			7
#define SYSCALL_CLOSE			8
#define SYSCALL_SETVIDEO		9
#define SYSCALL_SWAP_FRONTBUFFER	10
#define SYSCALL_PLAYSOUND		11
#define SYSCALL_GETMOUSE		12 /* XXX: not implemented */
#define SYSCALL_GETKEYMODE		13

#define NR_SYSCALL			(SYSCALL_GETKEYMODE + 1)

extern volatile void *brk;

#endif				/* !KSTD_H_ */
