#ifndef STDIO_H
#define STDIO_H

#include <k/kstd.h>

volatile int k_open(const char *, int);
volatile int k_close(int);
volatile ssize_t k_read(int, void *, size_t);
volatile off_t k_seek(int, off_t, int);

#endif /* STDIO_H */