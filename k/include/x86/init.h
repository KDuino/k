#ifndef INIT_H
#define INIT_H

void init_PIC(void);
void init_serial(void);
void init_isr(void);
void init_pe_mode(void);
void init_gdt(void);
void init_PIT(void);

#endif  // INIT_H