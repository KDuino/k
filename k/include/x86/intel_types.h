#ifndef INTEL_TYPES_H
#define INTEL_TYPES_H
#include <stdint.h>

typedef struct GDT {
  uint64_t limit1 : 16;
  uint64_t base1 : 24;
  uint64_t type : 4;
  uint64_t descriptor : 1;
  uint64_t level : 2;
  uint64_t present : 1;
  uint64_t limit2 : 4;
  uint64_t available : 1;
  uint64_t reserved : 1;
  uint64_t dbit : 1;
  uint64_t granularity : 1;
  uint64_t base2 : 8;
} GDT;

typedef struct GDTR {
  uint64_t limit : 16;
  uint64_t base : 32;
  uint64_t na : 16;
} GDTR;

typedef struct IDT {
  uint64_t offset1 : 16;
  uint64_t segment : 16;
  uint64_t unused : 8;
  uint64_t type : 4;
  uint64_t storage : 1;
  uint64_t dpl : 2;
  uint64_t present : 1;
  uint64_t offset2 : 16;
} IDT;

typedef struct IDT TaskGate;
typedef struct IDT InterruptGate;
typedef struct IDT TrapGate;

typedef struct IDTR {
  uint64_t limit : 16;
  uint64_t base : 32;
  uint64_t na : 16;
} IDTR;

typedef struct ICW1 {
  uint8_t icw4 : 1;
  uint8_t cascade : 1;
  uint8_t ivecSize : 1;
  uint8_t mode : 1;
  uint8_t mask : 4;
} ICW1;

#endif  // INTEL_TYPES_H