#ifndef KEYBOARD_H
#define KEYBOARD_H
#include <stdint.h>
#include <stdbool.h>

static inline bool is_key_released(uint8_t key_code) {
  return key_code & 0b10000000;
}

static inline bool is_key_pressed(uint8_t key_code) {
  return !is_key_released(key_code);
}

static inline uint8_t get_key_code(uint8_t key_code) {
  return key_code & 0b01111111;
}

char ascii_read(void);
int k_getkey(void);

#endif /* KEYBOARD_H */