#ifndef MACROS_H
#define MACROS_H
#include <stdbool.h>
#include <stdint.h>

#define GDT_NB_ENTRIES 8192
#define NULL_SEGMENT \
  { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 }
#define KCODE_SEGMENT \
  { 0xFFFF, 0x0, 0xA, 0x1, 0x0, 0x1, 0xF, 0x1, 0x1, 0x1, 0x1, 0x0 }
#define KDATA_SEGMENT \
  { 0xFFFF, 0x0, 0x2, 0x1, 0x0, 0x1, 0xF, 0x1, 0x1, 0x1, 0x1, 0x0 }
#define UCODE_SEGMENT \
  { 0xFFFF, 0x0, 0xA, 0x1, 0x3, 0x1, 0xF, 0x1, 0x1, 0x1, 0x1, 0x0 }
#define UDATA_SEGMENT \
  { 0xFFFF, 0x0, 0x2, 0x1, 0x3, 0x1, 0xF, 0x1, 0x1, 0x1, 0x1, 0x0 }

#define DECLARE_ICW1_MASTER 0x11
#define DECLARE_ICW1_SLAVE 0x11
#define DECLARE_ICW2_MASTER 0x20
#define DECLARE_ICW2_SLAVE 0x28
#define DECLARE_ICW3_MASTER 0x4
#define DECLARE_ICW3_SLAVE 0x2
#define DECLARE_ICW4_8086 0x1
#define DECLARE_ICW4_AUTO_EOI 0x2
#define DECLARE_ICW4_BUFFERED_SLAVE 0x8
#define DECLARE_ICW4_BUFFERED_MASTER 0xC
#define DECLARE_ICW4_FULLY_NESTED 0x10

#define PIC_MASTER_PORT_A 0x20
#define PIC_SLAVE_PORT_A 0xA0
#define PIC_MASTER_PORT_B 0x21
#define PIC_SLAVE_PORT_B 0xA1

#define PIC_END_OF_INTERRUPT 0x20

#define COM1 0x3F8

#define IRQ_OFFSET 32
#define IRQ0 IRQ_OFFSET
#define IRQ1 IRQ_OFFSET + 1
#define IRQ2 IRQ_OFFSET + 2
#define IRQ3 IRQ_OFFSET + 3
#define IRQ4 IRQ_OFFSET + 4
#define IRQ5 IRQ_OFFSET + 5
#define IRQ6 IRQ_OFFSET + 6
#define IRQ7 IRQ_OFFSET + 7
#define IRQ8 IRQ_OFFSET + 8
#define IRQ9 IRQ_OFFSET + 9
#define IRQ10 IRQ_OFFSET + 10
#define IRQ11 IRQ_OFFSET + 11
#define IRQ12 IRQ_OFFSET + 12
#define IRQ13 IRQ_OFFSET + 13
#define IRQ14 IRQ_OFFSET + 14
#define IRQ15 IRQ_OFFSET + 15

#define PIT_IRQ IRQ0
#define KBD_IRQ IRQ1
#define NOP_IRQ 42

#define PIT_CHANNEL0_DATA_PORT  0x40
#define PIT_CHANNEL1_DATA_PORT  0x41
#define PIT_CHANNEL2_DATA_PORT  0x42
#define PIT_COMMAND_PORT        0x43

#define PIT_CHANNEL0_INIT 0b00110100
#define PIT_CHANNEL0_DIVIDER 11932UL

#define SYSCALL_IRQ 0x80

#define IDT_NB_ELEMENTS 255

static inline uint64_t get_flags(void) {
  uint64_t flags = 0;
  asm volatile(
    "pushf\n"
    "pop %0\n"
    : "=r"(flags)
  );
  return flags;
}

static inline bool interrupts_enabled() {
  return get_flags() & 0b1000000000;
}

static inline bool interrupts_disabled() {
  return !interrupts_enabled();
}

static inline void enable_interrupts(void) {
  if (interrupts_disabled()) {
    asm volatile(
      "sti\n"
      "nop\n"
    );
    while (interrupts_disabled());
  }
}

static inline void disable_interrupts(void) {
  if (interrupts_enabled()) {
    asm volatile("cli\n");
    while (interrupts_enabled());
  }
}

#define ATOMIC(code) \
  disable_interrupts(); \
  code \
  enable_interrupts();

#endif  // MACROS_H