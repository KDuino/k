#ifndef INTERRUPTS_H
#define INTERRUPTS_H
#include <stdint.h>

typedef void (*pic_callback)(void);
typedef volatile uint32_t (*syscall_callback)(int, ...);

volatile uint32_t end_of_interrupt(uint8_t, uint32_t *);
volatile uint32_t interrupt_handler(uint32_t *);

volatile void keyboard_controller(void);
volatile void pit_controller(void);

#endif  // INTERRUPTS_H