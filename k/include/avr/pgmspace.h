#ifndef PGMSPACE_H
#define PGMSPACE_H

#define PROGMEM
#define PSTR(str) str
#define F(str) str
#define __FlashStringHelper char

#endif  // PGMSPACE_H