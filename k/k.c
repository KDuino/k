/*
 * Copyright (c) LSE
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY LSE AS IS AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL LSE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <k/kstd.h>
#include <x86/keyboard.h>
#include <x86/init.h>
#include <x86/intel_types.h>
#include <x86/macros.h>
#include <x86/millis.h>
#include <stdio.h>
#include <stdint.h>
#include "io.h"
#include "multiboot.h"

volatile void k_main(unsigned long magic, multiboot_info_t *info) {
  (void)magic;
  (void)info;

  ATOMIC({
    init_serial();
    init_gdt();
    init_pe_mode();
    init_isr();
    init_PIT();
    init_PIC();

    handle_multiboot(info);
  })

  char star[4] = "|/-\\";
  char *fb = (void *)0xb8000;

  ATOMIC({
    uint8_t buf[4096];
    int fd = open("blue.bmp", O_RDONLY);
    printf("blue.bmp fd = %d\n", fd);
    printf("Read %d bytes on blue.bmp file\n", read(fd, buf, 4096));
    printf("Going back to the start of the file: offset = %d\n", seek(fd, 0, SEEK_SET));
    printf("Moving 42 bytes forward in the file: offset = %d\n", seek(fd, 42, SEEK_CUR));
    printf("Going to the end of the file: offset = %d\n", seek(fd, 0, SEEK_END));
    printf("Moving to the 512th byte of the file: offset = %d\n", seek(fd, 512, SEEK_SET));
    printf("Closing blue.bmp file: ret = %d\n", close(fd));
  })

  for (unsigned i = 0;;) {
    ATOMIC({
      *fb = star[i++ % 4];
      //printf("millis = %lu\n", millis());
      int key = k_getkey();
      if (key != -1) {
        printf("key = %d\n", key);
      }
    })
    asm volatile("hlt\n");
  }

  for (;;) asm volatile("hlt\n");
}
