#include <stddef.h>
#include <ctype.h>
#include <k/kstd.h>
#include <x86/keyboard.h>
#include "io.h"
#define KEYBOARD_BUFFER_SIZE 64

static volatile uint8_t keyboard_buffer[KEYBOARD_BUFFER_SIZE] = {0};
static volatile size_t keyboard_write_idx = 0;
static volatile size_t keyboard_read_idx = 0;
static volatile bool keyboard_maj_lock = false;

volatile int k_getkey(void) {
  int key = -1;
  if (keyboard_read_idx != keyboard_write_idx) {
    key = keyboard_buffer[keyboard_read_idx++];
    if (keyboard_read_idx >= KEYBOARD_BUFFER_SIZE) {
      keyboard_read_idx = 0;
    }
  }
  return key;
}

static inline char keycode_to_char(uint8_t keycode) {
  switch(keycode) {
    case KEY_SPACE:
      return ' ';
    case KEY_BACKSPACE:
      return '\x7F';
    case KEY_A:
      return 'A';
    case KEY_B:
      return 'B';
    case KEY_C:
      return 'C';
    case KEY_D:
      return 'D';
    case KEY_E:
      return 'E';
    case KEY_F:
      return 'F';
    case KEY_G:
      return 'G';
    case KEY_H:
      return 'H';
    case KEY_I:
      return 'I';
    case KEY_J:
      return 'J';
    case KEY_K:
      return 'K';
    case KEY_L:
      return 'L';
    case KEY_M:
      return 'M';
    case KEY_N:
      return 'N';
    case KEY_O:
      return 'O';
    case KEY_P:
      return 'P';
    case KEY_Q:
      return 'Q';
    case KEY_R:
      return 'R';
    case KEY_S:
      return 'S';
    case KEY_T:
      return 'T';
    case KEY_U:
      return 'U';
    case KEY_V:
      return 'V';
    case KEY_W:
      return 'W';
    case KEY_X:
      return 'X';
    case KEY_Y:
      return 'Y';
    case KEY_Z:
      return 'Z';
    case KEY_OPEN_BRACKET:
      return '[';
    case KEY_BACK_SLASH:
      return '\\';
    case KEY_CLOSE_BRACKET:
      return ']';
    default:
      return '\0';
  }
}

static inline char keycode_to_ascii(uint8_t keycode, uint8_t keycode2) {
  if (keycode == KEY_CTRL) {
    switch (keycode2) {
      case KEY_A:
        return '\x01';
      case KEY_B:
        return '\x02';
      case KEY_C:
        return '\x03';
      case KEY_D:
        return '\x04';
      case KEY_E:
        return '\x05';
      case KEY_F:
        return '\x06';
      case KEY_G:
        return '\x07';
      case KEY_H:
        return '\x08';
      case KEY_I:
        return '\x09';
      case KEY_J:
        return '\x0A';
      case KEY_K:
        return '\x0B';
      case KEY_L:
        return '\x0C';
      case KEY_M:
        return '\x0D';
      case KEY_N:
        return '\x0E';
      case KEY_O:
        return '\x0F';
      case KEY_P:
        return '\x10';
      case KEY_Q:
        return '\x11';
      case KEY_R:
        return '\x12';
      case KEY_S:
        return '\x13';
      case KEY_T:
        return '\x14';
      case KEY_U:
        return '\x15';
      case KEY_V:
        return '\x16';
      case KEY_W:
        return '\x17';
      case KEY_X:
        return '\x18';
      case KEY_Y:
        return '\x19';
      case KEY_Z:
        return '\x1A';
      case KEY_OPEN_BRACKET:
        return '\x1B';
      case KEY_BACK_SLASH:
        return '\x1C';
      case KEY_CLOSE_BRACKET:
        return '\x1D';
      /*case KEY_CHEVRON:
        return '\x1E';
      case KEY_UNDERSCORE:
        return '\x1F';*/
      default:
        return '\0';
    }
  } else if (keycode == KEY_MAJLOCK) {
    keyboard_maj_lock = !keyboard_maj_lock;
  } else if (keyboard_maj_lock) {
    return keycode_to_char(keycode);
  } else {
    return tolower(keycode_to_char(keycode));
  }
  return '\0';
}

char volatile ascii_read(void) {
  uint8_t in1 = '\0', in2 = '\0';
  while ((in1 = k_getkey()) != '\0') {
    uint8_t tmp = get_key_code(in1);
    if (tmp != KEY_CTRL && is_key_released(in1)) {
      break;
    }
    if (in1 == KEY_CTRL) {
      while ((in2 = k_getkey()) == KEY_CTRL);
      if ((in2 = get_key_code(in2)) == KEY_CTRL) {
        in2 = '\0';
        in1 = KEY_CTRL;
      }
      break;
    }
  }
  in1 = get_key_code(in1);
  return keycode_to_ascii(in1, in2);
}

volatile void keyboard_controller(void) {
  uint8_t key = inb(0x60);
  keyboard_buffer[keyboard_write_idx++] = key;
  if (keyboard_write_idx >= KEYBOARD_BUFFER_SIZE) {
    keyboard_write_idx = 0;
  }
  //printf("%c\n", keycode_to_char(key));
}